

import java.util.ArrayList;

public class Library {
	public static final int FEE = 10;
	public static final int DUE_DATE_ENG = 14;
	public static final int DUE_DATE_THAI = 7;
	ArrayList<String> allbook = new ArrayList<String>();
	ArrayList<String> allborrower = new ArrayList<String>();
	private int money;

	public int bookCount() {
		return allbook.size();
	}

	public void borrowBook(int d, int m, int y, Student student1, Book book) {
		book.d = d;
		book.m = m;
		book.y = y;
		allbook.add(book.bookname);
		allborrower.add(student1.name);
	}

	public void borrowBook(int d, int m, int y, Professor professor1, Book book) {
		book.d = d;
		book.m = m;
		book.y = y;
		allbook.add(book.bookname);
		allborrower.add(professor1.name);
	}

	public void borrowBook(int d, int m, int y, Staff staff1, Book book) {
		book.d = d;
		book.m = m;
		book.y = y;
		allbook.add(book.bookname);
		allborrower.add(staff1.name);

	}

	public void getBorrower() {
		System.out.println("Name of borrower : ");
		for (int i = 0; i < allborrower.size(); i++) {
			System.out.println(allborrower.get(i));
		}

	}

	public void getBorrowBook() {
		System.out.println("Name of book is borrowed : ");
		for (int i = 0; i < allbook.size(); i++) {
			System.out.println(allbook.get(i));
		}
	}

	public int returnBook(int dd, int mm, int yy, Book book, Student student) {
		int money = 0;
		int allday = dd - book.d;
		for (int i = 0; i < allbook.size(); i++) {
			if (allbook.get(i) == book.bookname) {
				allbook.remove(i);
			}
		}
		for (int i = 0; i < allborrower.size(); i++) {
			if (allborrower.get(i) == student.name) {
				allborrower.remove(i);
			}
		}
		if ((book.booktype.equals("0") && allday > DUE_DATE_THAI)){
			money = (allday-DUE_DATE_THAI) * FEE;
		}
		else if ((book.booktype.equals("1") && allday > DUE_DATE_ENG)){
			money = (allday-DUE_DATE_ENG) * FEE;
		}
		return money;
	}

	public int returnBook(int dd, int mm, int yy, Book book, Professor professor) {
		int money = 0;
		int allday = dd - book.d;
		for (int i = 0; i < allbook.size(); i++) {
			if (allbook.get(i) == book.bookname) {
				allbook.remove(i);
			}
		}
		for (int i = 0; i < allborrower.size(); i++) {
			if (allborrower.get(i) == professor.name) {
				allborrower.remove(i);
			}
		}
		if ((book.booktype.equals("0") && allday > DUE_DATE_THAI)){
			money = (allday-DUE_DATE_THAI) * FEE;
		}
		else if ((book.booktype.equals("1") && allday > DUE_DATE_ENG)){
			money = (allday-DUE_DATE_ENG) * FEE;
		}
		return money;
	}
	
	public int returnBook(int dd, int mm, int yy, Book book, Staff staff) {
		int money = 0;
		int allday = dd - book.d;
		for (int i = 0; i < allbook.size(); i++) {
			if (allbook.get(i) == book.bookname) {
				allbook.remove(i);
			}
		}
		for (int i = 0; i < allborrower.size(); i++) {
			if (allborrower.get(i) == staff.name) {
				allborrower.remove(i);
			}
		}
		if ((book.booktype.equals("0") && allday > DUE_DATE_THAI)){
			money = (allday-DUE_DATE_THAI) * FEE;
		}
		else if ((book.booktype.equals("1") && allday > DUE_DATE_ENG)){
			money = (allday-DUE_DATE_ENG) * FEE;
		}
		return money;
	}

}
