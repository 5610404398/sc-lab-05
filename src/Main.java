import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Library l = new Library();
		Student stu = new Student("Toey","Thani","5610404888");
		Professor profess = new Professor("Hahaha","HeHeHe","D14");
		Staff worker = new Staff("Mistery");
		Book book1 = new Book("Go Out","0", "522"); // 0 is thai book
		Book book2 = new Book("Big Java","1","333"); // 1 is english book
		Book book3 = new Book("Small Java","1","222"); // 1 is english book
		
		System.out.println("Number of book : ");
		System.out.println(l.bookCount());
		l.borrowBook(13, 2, 2558, stu, book1);
		l.getBorrower();
		l.getBorrowBook();
		
		//profess borrower book
		l.borrowBook(1, 2, 2558, profess, book2);
		l.getBorrower();
		l.getBorrowBook();
		
		System.out.println("Number of book : ");
		System.out.println(l.bookCount());
		
		l.borrowBook(18, 2, 2558, worker, book3);
		l.getBorrower();
		l.getBorrowBook();
		
		System.out.println("You must pay for fine "+l.returnBook(30, 2, 2558, book1, stu)+" baht.");
		l.getBorrower();
		l.getBorrowBook();
		System.out.println("You must pay for fine "+l.returnBook(16, 2, 2558,book2, profess)+" baht.");
		System.out.println("You must pay for fine "+l.returnBook(25, 2, 2558, book3, worker)+" baht.");
		
		
	}
}
